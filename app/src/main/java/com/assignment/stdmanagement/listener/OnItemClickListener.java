package com.assignment.stdmanagement.listener;


import java.util.List;

public interface OnItemClickListener<T> {
    void onClick(Object object);
    void onSelection(List<T> selected);
}
