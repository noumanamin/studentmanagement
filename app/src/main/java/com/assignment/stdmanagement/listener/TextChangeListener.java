package com.assignment.stdmanagement.listener;

public interface TextChangeListener {
    void onTextChanged(boolean isStudentExist);
}
