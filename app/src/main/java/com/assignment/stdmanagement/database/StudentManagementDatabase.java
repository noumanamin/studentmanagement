package com.assignment.stdmanagement.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.assignment.stdmanagement.dao.ExamDao;
import com.assignment.stdmanagement.dao.StudentDao;
import com.assignment.stdmanagement.dao.TodoDao;
import com.assignment.stdmanagement.model.Exam;
import com.assignment.stdmanagement.model.Student;
import com.assignment.stdmanagement.model.Todo;

@Database(entities = {Todo.class, Student.class, Exam.class},
        version = 1, exportSchema = false)
public abstract class StudentManagementDatabase extends RoomDatabase {

    private static StudentManagementDatabase instance;

    public abstract StudentDao studentDao();
    public abstract ExamDao examDao();
    public abstract TodoDao todoDao();

    public static synchronized StudentManagementDatabase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    StudentManagementDatabase.class,"student_management_db").
                    addCallback(roomCalback).
                    fallbackToDestructiveMigration().build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCalback = new RoomDatabase.Callback(){

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        }
    };
}

