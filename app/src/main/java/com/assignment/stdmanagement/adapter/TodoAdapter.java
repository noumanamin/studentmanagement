package com.assignment.stdmanagement.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.listener.OnItemClickListener;
import com.assignment.stdmanagement.listener.OnStatusChangeListener;
import com.assignment.stdmanagement.model.Todo;

import java.util.List;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoViewHolder> {

    private List<Todo> todos;
    private LayoutInflater layoutInflater;
    private OnItemClickListener onitemClickListenner;
    private OnStatusChangeListener onStatusChangeListener;

    public TodoAdapter(List<Todo> todos, OnItemClickListener onitemClickListenner,
                       OnStatusChangeListener onStatusChangeListener) {
        this.todos = todos;
        this.onitemClickListenner = onitemClickListenner;
        this.onStatusChangeListener = onStatusChangeListener;
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int position) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        final View view = layoutInflater.inflate(R.layout.item_todo, parent, false);
        return new TodoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TodoViewHolder todoViewHolder, final int position) {
        final Todo todo = todos.get(position);
        todoViewHolder.tvAddress.setText(todo.getLocation());
        todoViewHolder.tvTodoTask.setText(todo.getTaskName());
        todoViewHolder.switchStatus.setChecked(todo.isStatus());
        todoViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onitemClickListenner.onClick(todo);
            }
        });
        todoViewHolder.switchStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onStatusChangeListener.onStatusChange(getTodo(todoViewHolder.getAdapterPosition()), isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return todos == null ? 0 : todos.size();
    }

    public void updateTodos(List<Todo> students) {
        this.todos = students;
        notifyDataSetChanged();
    }

    public Todo getTodo(int position) {
        return todos.get(position);
    }


    class TodoViewHolder extends RecyclerView.ViewHolder {
        TextView tvTodoTask;
        TextView tvAddress;
        Switch switchStatus;

        TodoViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAddress = itemView.findViewById(R.id.tvLocation);
            tvTodoTask = itemView.findViewById(R.id.tvTodoTask);
            switchStatus = itemView.findViewById(R.id.switchStatus);
        }
    }
}

