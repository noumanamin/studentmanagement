package com.assignment.stdmanagement.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.listener.OnItemClickListener;
import com.assignment.stdmanagement.model.Exam;

import java.util.List;

public class ExamsAdapter extends RecyclerView.Adapter<ExamsAdapter.StudentViewHolder> {

    private List<Exam> exams;
    private LayoutInflater layoutInflater;
    private OnItemClickListener onitemClickListener;

    public ExamsAdapter(List<Exam> exams, OnItemClickListener onitemClickListener) {
        this.exams = exams;
        this.onitemClickListener = onitemClickListener;
    }

    @NonNull
    @Override
    public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int position) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        return new StudentViewHolder(layoutInflater.inflate(R.layout.item_exam, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentViewHolder studentViewHolder, int position) {
        final Exam exam = exams.get(position);
        studentViewHolder.tvExamName.setText(exam.getExamName());
        studentViewHolder.tvDateTime.setText(String.format("%s %s", exam.getDate(), exam.getTime()));
        studentViewHolder.tvLocation.setText(exam.getLocation());

        /*studentViewHolder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onitemClickListener != null) {
                    onitemClickListener.onClick(exam);
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return exams == null ? 0 : exams.size();
    }

    public void updateExam(List<Exam> exams) {
        this.exams = exams;
        notifyDataSetChanged();
    }

    public Exam getExam(int position) {
        return exams.get(position);
    }

    class StudentViewHolder extends RecyclerView.ViewHolder {
        CardView main;
        TextView tvExamName;
        TextView tvDateTime;
        TextView tvLocation;
        CheckBox cbSelected;

        StudentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDateTime = itemView.findViewById(R.id.tvDateTime);
            tvExamName = itemView.findViewById(R.id.tvExamName);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            main = itemView.findViewById(R.id.main);
            cbSelected = itemView.findViewById(R.id.cbSelected);
        }
    }
}

