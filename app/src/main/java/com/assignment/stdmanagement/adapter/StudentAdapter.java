package com.assignment.stdmanagement.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.listener.OnItemClickListener;
import com.assignment.stdmanagement.model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentViewHolder> {

    private List<Student> students;
    private LayoutInflater layoutInflater;
    private OnItemClickListener onitemClickListener;

    public StudentAdapter(List<Student> students, OnItemClickListener onitemClickListener) {
        this.students = students;
        this.onitemClickListener = onitemClickListener;
    }

    @NonNull
    @Override
    public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int position) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        return new StudentViewHolder(layoutInflater.inflate(R.layout.item_student, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentViewHolder studentViewHolder, int position) {
        final Student student = students.get(position);
        studentViewHolder.tvName.setText(String.format("%s %s", student.getFirstName(), student.getLastName()));
        studentViewHolder.tvAge.setText(student.getAge());
        studentViewHolder.tvGender.setText(student.getGender());
        studentViewHolder.tvAddress.setText(student.getAddress());

        studentViewHolder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onitemClickListener != null) {
                    onitemClickListener.onClick(student);
                }
            }
        });

        studentViewHolder.cbSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                students.get(studentViewHolder.getAdapterPosition()).setSelected(isChecked);
                if(onitemClickListener != null){
                    onitemClickListener.onSelection(getSelectedStudent());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return students == null ? 0 : students.size();
    }

    public void updateStudent(List<Student> students) {
        this.students = students;
        notifyDataSetChanged();
    }

    public List<Student> getSelectedStudent(){
        List<Student> selectedStudents = new ArrayList<>();
        for (Student student: students) {
            if(student.isSelected()){
                selectedStudents.add(student);
            }
        }
        return selectedStudents;
    }

    public Student getStudent(int position) {
        return students.get(position);
    }


    class StudentViewHolder extends RecyclerView.ViewHolder {
        CardView main;
        TextView tvName;
        TextView tvGender;
        TextView tvAge;
        TextView tvAddress;
        CheckBox cbSelected;

        StudentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAddress = itemView.findViewById(R.id.tvLocation);
            tvAge = itemView.findViewById(R.id.tvAge);
            tvGender = itemView.findViewById(R.id.tvGender);
            tvName = itemView.findViewById(R.id.tvExamName);
            main = itemView.findViewById(R.id.main);
            cbSelected = itemView.findViewById(R.id.cbSelected);
        }
    }
}
