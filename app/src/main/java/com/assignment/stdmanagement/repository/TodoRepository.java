package com.assignment.stdmanagement.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.assignment.stdmanagement.dao.TodoDao;
import com.assignment.stdmanagement.database.StudentManagementDatabase;
import com.assignment.stdmanagement.model.Todo;
import com.assignment.stdmanagement.utils.DbOprationType;

import java.util.List;

public class TodoRepository {

    private TodoDao todoDao;
    private LiveData<List<Todo>> todos;

    public TodoRepository(Application application) {
        StudentManagementDatabase database = StudentManagementDatabase.getInstance(application);
        todoDao = database.todoDao();
        todos = todoDao.getAllTodo();
    }

    public void insert(Todo todo) {
        new TodoAsyncTask(todoDao, DbOprationType.INSERT).execute(todo);
    }

    public void update(Todo todo) {
        new TodoAsyncTask(todoDao, DbOprationType.UPDATE).execute(todo);
    }

    public void delete(Todo todo) {
        new TodoAsyncTask(todoDao, DbOprationType.DELETE).execute(todo);
    }

    public LiveData<List<Todo>> getTodos() {
        return todos;
    }

    public LiveData<List<Todo>> getCompletedTodos() {
        return todoDao.getAllTodo(true);
    }

    public LiveData<List<Todo>> getUnCompletedTodos() {
        return todoDao.getAllTodo(false);
    }

    public void deleteAllTodo(boolean isCompleted) {
        TodoAsyncTask todoAsyncTask = new TodoAsyncTask(todoDao, DbOprationType.DELETE_ALL);
        todoAsyncTask.isCompleted = isCompleted;
        todoAsyncTask.execute();
    }

    private static class TodoAsyncTask extends AsyncTask<Todo, Void, Void> {

        private TodoDao todoDao;
        private int dbOptionType;
        public boolean isCompleted;

        private TodoAsyncTask(TodoDao todoDao, int dbOptionType) {
            this.todoDao = todoDao;
            this.dbOptionType = dbOptionType;
        }

        @Override
        protected Void doInBackground(Todo... todos) {
            switch (dbOptionType) {
                case DbOprationType.INSERT:
                    todoDao.insert(todos[0]);
                    break;
                case DbOprationType.UPDATE:
                    todoDao.update(todos[0]);
                    break;
                case DbOprationType.DELETE:
                    todoDao.delete(todos[0]);
                    break;
                case DbOprationType.DELETE_ALL:
                    todoDao.deleteAllTodos(isCompleted);
                    break;
            }
            return null;
        }
    }
}
