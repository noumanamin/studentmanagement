package com.assignment.stdmanagement.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.assignment.stdmanagement.dao.ExamDao;
import com.assignment.stdmanagement.database.StudentManagementDatabase;
import com.assignment.stdmanagement.model.Exam;
import com.assignment.stdmanagement.utils.DbOprationType;

import java.util.List;

public class ExamRepository {
    private ExamDao examDao;
//    private LiveData<List<Exam>> exams;
    public ExamRepository(Application application) {
        StudentManagementDatabase database = StudentManagementDatabase.getInstance(application);
        examDao = database.examDao();

    }

    public void insert(Exam exam) {
        new ExamAsyncTask(examDao, DbOprationType.INSERT).execute(exam);
    }

    public void update(Exam exam) {
        new ExamAsyncTask(examDao, DbOprationType.UPDATE).execute(exam);
    }

    public void delete(Exam exam) {
        new ExamAsyncTask(examDao, DbOprationType.DELETE).execute(exam);
    }

    public LiveData<List<Exam>> getExamByStudentId(String studentId){
        return examDao.getAllExamByStudentId(studentId);
    }

    private static class ExamAsyncTask extends AsyncTask<Exam, Void, Void> {

        private ExamDao examDao;
        private int dbOptionType;

        private ExamAsyncTask(ExamDao examDao, int dbOptionType) {
            this.examDao = examDao;
            this.dbOptionType = dbOptionType;
        }

        @Override
        protected Void doInBackground(Exam... exams) {
            switch (dbOptionType) {
                case DbOprationType.INSERT:
                    examDao.insert(exams[0]);
                    break;
                case DbOprationType.UPDATE:
                    examDao.update(exams[0]);
                    break;
                case DbOprationType.DELETE:
                    examDao.delete(exams[0]);
                    break;
            }
            return null;
        }
    }
}
