package com.assignment.stdmanagement.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.assignment.stdmanagement.dao.StudentDao;
import com.assignment.stdmanagement.database.StudentManagementDatabase;
import com.assignment.stdmanagement.model.Student;
import com.assignment.stdmanagement.utils.DbOprationType;

import java.util.List;

public class StudentRepository {
    private StudentDao studentDao;
    private LiveData<List<Student>> students;

    public StudentRepository(Application application) {
        StudentManagementDatabase database = StudentManagementDatabase.getInstance(application);
        studentDao = database.studentDao();
        students = studentDao.getAllStudent();
    }

    public void insert(Student student) {
        new StudentAsyncTask(studentDao, DbOprationType.INSERT).execute(student);
    }

    public void update(Student student) {
        new StudentAsyncTask(studentDao, DbOprationType.UPDATE).execute(student);
    }

    public void delete(Student student) {
        new StudentAsyncTask(studentDao, DbOprationType.DELETE).execute(student);
    }

    public void deleteAllStudents() {
        new StudentAsyncTask(studentDao, DbOprationType.DELETE_ALL).execute();
    }

    public LiveData<List<Student>> getStudents() {
        return students;
    }

    public LiveData<Student> getStudentById(String studentId) {
        return studentDao.getStudentById(studentId);
    }

    private static class StudentAsyncTask extends AsyncTask<Student, Void, Void> {

        private StudentDao studentDao;
        private int dbOptionType;

        private StudentAsyncTask(StudentDao studentDao, int dbOptionType) {
            this.studentDao = studentDao;
            this.dbOptionType = dbOptionType;
        }

        @Override
        protected Void doInBackground(Student... students) {
            switch (dbOptionType) {
                case DbOprationType.INSERT:
                    studentDao.insert(students[0]);
                    break;
                case DbOprationType.UPDATE:
                    studentDao.update(students[0]);
                    break;
                case DbOprationType.DELETE:
                    studentDao.delete(students[0]);
                    break;
                case DbOprationType.DELETE_ALL:
                    studentDao.deleteAllStudent();
                    break;
            }
            return null;
        }
    }

}
