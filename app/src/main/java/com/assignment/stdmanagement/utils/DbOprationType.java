package com.assignment.stdmanagement.utils;

public interface DbOprationType {

    int INSERT = 1;
    int UPDATE = 2;
    int DELETE = 3;
    int GET = 4;
    int DELETE_ALL = 5;
}
