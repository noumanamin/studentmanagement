package com.assignment.stdmanagement.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.assignment.stdmanagement.model.Student;

import java.util.List;

@Dao
public interface StudentDao {

    @Insert
    void insert(Student student);

    @Update
    void update(Student student);

    @Delete
    void delete(Student student);

    @Query("DELETE FROM student")
    void deleteAllStudent();

    @Query("SELECT * FROM student")
    LiveData<List<Student>> getAllStudent();

    @Query("SELECT * FROM student WHERE student_id = :studentId")
    LiveData<Student> getStudentById(String studentId);


}
