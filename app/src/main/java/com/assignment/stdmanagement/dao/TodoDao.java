package com.assignment.stdmanagement.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.assignment.stdmanagement.model.Todo;

import java.util.List;

@Dao
public interface TodoDao {

    @Insert
    void insert(Todo todo);

    @Update
    void update(Todo todo);

    @Delete
    void delete(Todo todo);

    @Query("SELECT * FROM todo")
    LiveData<List<Todo>> getAllTodo();

    @Query("SELECT * FROM todo WHERE status = :isCompleted")
    LiveData<List<Todo>> getAllTodo(boolean isCompleted);

    @Query("DELETE FROM todo WHERE status = :isCompleted")
    void deleteAllTodos(boolean isCompleted);
}
