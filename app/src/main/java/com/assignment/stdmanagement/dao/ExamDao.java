package com.assignment.stdmanagement.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.assignment.stdmanagement.model.Exam;

import java.util.List;

@Dao
public interface ExamDao {

    @Insert
    void insert(Exam exam);

    @Update
    void update(Exam exam);

    @Delete
    void delete(Exam exam);

    @Query("SELECT * FROM exam WHERE student_id = :studentId")
    LiveData<List<Exam>> getAllExamByStudentId(String studentId);
}
