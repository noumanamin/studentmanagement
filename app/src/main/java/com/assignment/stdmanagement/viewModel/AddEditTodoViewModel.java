package com.assignment.stdmanagement.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.assignment.stdmanagement.databinding.ActivityAddEditTodoBinding;
import com.assignment.stdmanagement.model.Todo;

public class AddEditTodoViewModel extends AndroidViewModel {
    private ActivityAddEditTodoBinding activityAddEditTodoBinding;

    public AddEditTodoViewModel(@NonNull Application application) {
        super(application);
    }

    public Todo getTodo() {
        Todo todo = new Todo();
        todo.setLocation(activityAddEditTodoBinding.edtAddress.getText().toString());
        todo.setStatus(activityAddEditTodoBinding.switchStatus.isChecked());
        todo.setTaskName(activityAddEditTodoBinding.edtTaskName.getText().toString());
        return todo;
    }

    public void setActivityAddEditTodoBinding(ActivityAddEditTodoBinding activityAddEditTodoBinding) {
        this.activityAddEditTodoBinding = activityAddEditTodoBinding;
    }
}
