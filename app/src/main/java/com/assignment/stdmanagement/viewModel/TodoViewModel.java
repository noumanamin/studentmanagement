package com.assignment.stdmanagement.viewModel;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.assignment.stdmanagement.view.todo.addEditTodo.AddEditTodoActivity;
import com.assignment.stdmanagement.model.Todo;
import com.assignment.stdmanagement.repository.TodoRepository;

import java.lang.ref.WeakReference;
import java.util.List;

import static com.assignment.stdmanagement.view.todo.TodoActivity.ADD_TODO_RESULT;

public class TodoViewModel extends AndroidViewModel {

    private TodoRepository todoRepository;
    private LiveData<List<Todo>> todos;
    private LiveData<List<Todo>> unCompletedTodos;
    private LiveData<List<Todo>> completedTodos;
    private WeakReference<Activity> activity;

    public TodoViewModel(@NonNull Application application) {
        super(application);
        todoRepository = new TodoRepository(application);
        todos = todoRepository.getTodos();
        unCompletedTodos = todoRepository.getUnCompletedTodos();
        completedTodos = todoRepository.getCompletedTodos();

    }

    public void setActivity(WeakReference<Activity> activity) {
        this.activity = activity;
    }

    public void insert(Todo todo) {
        todoRepository.insert(todo);
    }

    public void update(Todo todo) {
        todoRepository.update(todo);
    }

    public void delete(Todo todo) {
        todoRepository.delete(todo);
    }

    public LiveData<List<Todo>> getTodos() {
        return todos;
    }

    public LiveData<List<Todo>> getCompletedTodos() {
        return completedTodos;
    }

    public LiveData<List<Todo>> getUnCompletedTodos() {
        return unCompletedTodos;
    }

    public void onAddStudentClick() {
        Intent homeIntent = new Intent(activity.get(),
                AddEditTodoActivity.class);
        activity.get().startActivityForResult(homeIntent, ADD_TODO_RESULT);
    }

    public void deleteAllTodos(boolean isCompleted) {
        todoRepository.deleteAllTodo(isCompleted);
    }
}
