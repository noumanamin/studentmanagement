package com.assignment.stdmanagement.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;

import com.assignment.stdmanagement.databinding.ActivityAddEditStudentBinding;
import com.assignment.stdmanagement.listener.TextChangeListener;
import com.assignment.stdmanagement.model.Student;
import com.assignment.stdmanagement.repository.StudentRepository;

public class AddEditStudentViewModel extends AndroidViewModel {

    private ActivityAddEditStudentBinding activityAddEditStudentBinding;
    private StudentRepository studentRepository;
    private TextChangeListener textChangeListener;

    public AddEditStudentViewModel(@NonNull Application application) {
        super(application);
        studentRepository = new StudentRepository(application);
    }

    public void setActivityAddEditStudentBinding(ActivityAddEditStudentBinding activityAddEditStudentBinding) {
        this.activityAddEditStudentBinding = activityAddEditStudentBinding;
    }

    public void setTextChangeListener(TextChangeListener textChangeListener) {
        this.textChangeListener = textChangeListener;
    }

    public Student getStudent() {

        Student student = new Student();
        student.setStudentId(activityAddEditStudentBinding.edtStudentId.getText().toString());
        student.setAddress(activityAddEditStudentBinding.edtAddress.getText().toString());
        student.setAge(activityAddEditStudentBinding.edtAge.getText().toString());
        student.setFirstName(activityAddEditStudentBinding.edtFirstName.getText().toString());
        student.setLastName(activityAddEditStudentBinding.edtLastName.getText().toString());
        student.setGender(activityAddEditStudentBinding.spinnerGender.getSelectedItem().toString());
        student.setCourse(activityAddEditStudentBinding.edtCourseName.getText().toString());
        return student;
    }

    public Student getStudent(int id) {
        Student student = getStudent();
        student.setId(id);
        return student;

    }

    public TextWatcher getStudentIdTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Do nothing.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (activityAddEditStudentBinding.getLifecycleOwner() != null) {
                    studentRepository.getStudentById(s.toString()).observe(activityAddEditStudentBinding.getLifecycleOwner(), new Observer<Student>() {
                        @Override
                        public void onChanged(@Nullable Student student) {
                            if (student == null) {
                                textChangeListener.onTextChanged(false);
                            } else {
                                textChangeListener.onTextChanged(true);
                            }
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Do nothing.
            }
        };
    }

    public boolean isStudentValid() {
        return activityAddEditStudentBinding.edtStudentId.getError() == null
                && !activityAddEditStudentBinding.edtStudentId.getText().toString().equals("");
    }
}
