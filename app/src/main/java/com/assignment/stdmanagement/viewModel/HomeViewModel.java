package com.assignment.stdmanagement.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.assignment.stdmanagement.view.student.StudentActivity;
import com.assignment.stdmanagement.view.student.studentPhoto.StudentPhotoActivity;
import com.assignment.stdmanagement.view.todo.TodoActivity;

import java.lang.ref.WeakReference;

public class HomeViewModel extends AndroidViewModel {

    private WeakReference<Context> context;
    public HomeViewModel(@NonNull Application application) {
        super(application);
    }

    public void setContext(WeakReference<Context> context) {
        this.context = context;
    }

    public void onStudentClick() {
        startActivity(StudentActivity.class);
    }

    public void onTodoClick() {
        startActivity(TodoActivity.class);
    }

    public void onStudentPhotoClick() {
        startActivity(StudentPhotoActivity.class);
    }

    private void startActivity(Class aClass){
        Intent intent = new Intent(context.get(), aClass);
        context.get().startActivity(intent);
    }
}
