package com.assignment.stdmanagement.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;

import com.assignment.stdmanagement.databinding.ActivityAddEditExamBinding;
import com.assignment.stdmanagement.listener.TextChangeListener;
import com.assignment.stdmanagement.model.Exam;
import com.assignment.stdmanagement.model.Student;
import com.assignment.stdmanagement.repository.StudentRepository;

public class AddEditExamViewModel extends AndroidViewModel {

    private ActivityAddEditExamBinding activityAddEditExamBinding;
//    private Student student;
    private StudentRepository studentRepository;
    private TextChangeListener textChangeListener;

    public AddEditExamViewModel(@NonNull Application application) {
        super(application);
        studentRepository = new StudentRepository(application);
    }

    public void setActivityAddEditExamBinding(ActivityAddEditExamBinding activityAddEditExamBinding) {
        this.activityAddEditExamBinding = activityAddEditExamBinding;
    }

    public void setTextChangeListener(TextChangeListener textChangeListener) {
        this.textChangeListener = textChangeListener;
    }

    public Exam getExam() {
        Exam exam = new Exam();
        exam.setStudentId(activityAddEditExamBinding.edtStudentId.getText().toString());
        exam.setDate(activityAddEditExamBinding.edtDate.getText().toString());
        exam.setTime(activityAddEditExamBinding.edtTime.getText().toString());
        exam.setExamName(activityAddEditExamBinding.edtExamName.getText().toString());
        exam.setLocation(activityAddEditExamBinding.edtLocation.getText().toString());
        return exam;
    }

    public Exam getExam(int id) {
        Exam exam = getExam();
        exam.setId(id);
        return exam;
    }

    public TextWatcher getStudentIdTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Do nothing.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (activityAddEditExamBinding.getLifecycleOwner() != null) {
                    studentRepository.getStudentById(s.toString()).observe(activityAddEditExamBinding.getLifecycleOwner(), new Observer<Student>() {
                        @Override
                        public void onChanged(@Nullable Student student) {
                            if (student == null) {
                                textChangeListener.onTextChanged(false);
                            } else {
                                textChangeListener.onTextChanged(true);
                            }
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Do nothing.
            }
        };
    }

    public boolean isExamValid() {
        return activityAddEditExamBinding.edtStudentId.getError() == null
                && !activityAddEditExamBinding.edtStudentId.getText().toString().equals("");
    }

    public void setStudent(Student student) {
//        this.student = student;
    }
}