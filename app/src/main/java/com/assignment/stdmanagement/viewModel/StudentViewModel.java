package com.assignment.stdmanagement.viewModel;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.assignment.stdmanagement.view.student.addEditStudent.AddEditStudentActivity;
import com.assignment.stdmanagement.model.Student;
import com.assignment.stdmanagement.repository.StudentRepository;

import java.lang.ref.WeakReference;
import java.util.List;

import static com.assignment.stdmanagement.view.student.StudentActivity.ADD_STUDENT_RESULT;

public class StudentViewModel extends AndroidViewModel {

    private StudentRepository studentRepository;
    private LiveData<List<Student>> students;
    private WeakReference<Activity> activity;

    public StudentViewModel(@NonNull Application application) {
        super(application);
        studentRepository = new StudentRepository(application);
        students = studentRepository.getStudents();
    }

    public void setActivity(WeakReference<Activity> activity) {
        this.activity = activity;
    }

    public void insert(Student student) {
        studentRepository.insert(student);
    }

    public void update(Student student) {
        studentRepository.update(student);
    }

    public void delete(Student student) {
        studentRepository.delete(student);
    }

    public void deleteAllStudents(){
        studentRepository.deleteAllStudents();
    }

    public LiveData<List<Student>> getStudents() {
        return students;
    }


    public void onAddStudentClick(){
        Intent homeIntent = new Intent(activity.get(),
                AddEditStudentActivity.class);
        activity.get().startActivityForResult(homeIntent, ADD_STUDENT_RESULT);
    }
}
