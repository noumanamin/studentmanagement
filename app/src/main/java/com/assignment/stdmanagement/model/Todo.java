package com.assignment.stdmanagement.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "todo")
public class Todo implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String taskName;
    private String location;
    private boolean status;

    @Ignore
    public Todo(){

    }
    public Todo(String taskName, String location, boolean status) {
        this.taskName = taskName;
        this.location = location;
        this.status = status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getLocation() {
        return location;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
