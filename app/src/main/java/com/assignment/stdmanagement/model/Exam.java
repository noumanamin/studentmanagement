package com.assignment.stdmanagement.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "exam")
public class Exam implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "student_id")
    private String studentId;
    @ColumnInfo(name = "exam_name")
    private String examName;
    private String date;
    private String time;
    private String location;

    @Ignore
    public Exam() {
    }

    public Exam(String studentId, String examName, String date, String time, String location) {
        this.studentId = studentId;
        this.examName = examName;
        this.date = date;
        this.time = time;
        this.location = location;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getExamName() {
        return examName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
