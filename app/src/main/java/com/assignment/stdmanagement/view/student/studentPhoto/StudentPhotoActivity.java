package com.assignment.stdmanagement.view.student.studentPhoto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.assignment.stdmanagement.R;

public class StudentPhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_photo);
    }
}
