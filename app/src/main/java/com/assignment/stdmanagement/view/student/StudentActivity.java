package com.assignment.stdmanagement.view.student;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.adapter.StudentAdapter;
import com.assignment.stdmanagement.view.student.addEditStudent.AddEditStudentActivity;
import com.assignment.stdmanagement.databinding.ActivityStudentBinding;
import com.assignment.stdmanagement.listener.OnItemClickListener;
import com.assignment.stdmanagement.model.Student;
import com.assignment.stdmanagement.viewModel.StudentViewModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class StudentActivity extends AppCompatActivity implements OnItemClickListener {

    public static final int ADD_STUDENT_RESULT = 100;
    public static final int EDIT_STUDENT_RESULT = 101;
    public static final String KEY_STUDENT = "student_activity_student";

    private StudentViewModel studentViewModel;
    private StudentAdapter studentAdapter;
    private MenuItem delete;
    private MenuItem edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityStudentBinding activityStudentBinding = DataBindingUtil.
                setContentView(this, R.layout.activity_student);
        setTitle("Students");
        studentViewModel = ViewModelProviders.of(this).get(StudentViewModel.class);
        studentViewModel.setActivity(new WeakReference<Activity>(this));
        activityStudentBinding.setStudentViewModel(studentViewModel);
        setStudentObserver();
        setStudentAdapter(activityStudentBinding.rvStudent);
        addSwipeTouchListener(activityStudentBinding.rvStudent);
    }

    private void addSwipeTouchListener(RecyclerView rvStudents) {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                studentViewModel.delete(studentAdapter.getStudent(viewHolder.getAdapterPosition()));
                Toast.makeText(StudentActivity.this, "Student delete", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(rvStudents);
    }

    private void setStudentAdapter(RecyclerView rvStudent) {
        rvStudent.setHasFixedSize(true);
        rvStudent.setLayoutManager(new LinearLayoutManager(this));
        studentAdapter = new StudentAdapter(studentViewModel.getStudents().getValue(), this);
        rvStudent.setAdapter(studentAdapter);
    }

    private void setStudentObserver() {
        studentViewModel.getStudents().observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(@Nullable List<Student> students) {
                studentAdapter.updateStudent(students);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_STUDENT_RESULT) {
                Student student = null;
                if (data != null) {
                    student = (Student) data.getSerializableExtra(KEY_STUDENT);
                }
                studentViewModel.insert(student);
            } else if (requestCode == EDIT_STUDENT_RESULT) {
                Student student = null;
                if (data != null) {
                    student = (Student) data.getSerializableExtra(KEY_STUDENT);
                }
                edit.setVisible(false);
                delete.setVisible(false);
                studentViewModel.update(student);
            }
        }
    }

    @Override
    public void onClick(Object student) {
        Intent intent = new Intent(this, StudentViewActivity.class);
        intent.putExtra(KEY_STUDENT, (Student) student);
        startActivity(intent);
    }

    @Override
    public void onSelection(List selected) {
        ArrayList<Student> students = (ArrayList<Student>) selected;
        if (students.size() > 1) {
            delete.setVisible(true);
            edit.setVisible(false);
        } else if (students.size() == 1) {
            edit.setVisible(true);
            delete.setVisible(true);
        } else {
            edit.setVisible(false);
            delete.setVisible(false);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_delete, menu);
        delete = menu.findItem(R.id.delete);
        edit = menu.findItem(R.id.edit);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteAll:
                studentViewModel.deleteAllStudents();
                Toast.makeText(this, "Student has deleted", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.delete:
                ArrayList<Student> students = (ArrayList<Student>) studentAdapter.getSelectedStudent();
                for (Student student :
                        students) {
                    studentViewModel.delete(student);
                }
                studentAdapter.notifyDataSetChanged();
                edit.setVisible(false);
                delete.setVisible(false);
                Toast.makeText(this, "Selected student students has deleted", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.edit:
                Intent intent = new Intent(this, AddEditStudentActivity.class);
                Student student = studentAdapter.getSelectedStudent().get(0);
                intent.putExtra(KEY_STUDENT, student);
                startActivityForResult(intent, EDIT_STUDENT_RESULT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
