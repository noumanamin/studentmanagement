package com.assignment.stdmanagement.view.home;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.assignment.stdmanagement.BuildConfig;
import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.databinding.ActivityHomeBinding;
import com.assignment.stdmanagement.viewModel.HomeViewModel;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityHomeBinding activityHomeBinding = DataBindingUtil.
                setContentView(this, R.layout.activity_home);
        HomeViewModel homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.setContext(new WeakReference<Context>(this));
        activityHomeBinding.setHomeViewModel(homeViewModel);
        showDebugDBAddressLogToast(this);
    }

    public static void showDebugDBAddressLogToast(Context context) {
        if (BuildConfig.DEBUG) {
            try {
                Class<?> debugDB = Class.forName("com.assignment.stdmanagement.DebugDB");
                Method getAddressLog = debugDB.getMethod("getAddressLog");
                Object value = getAddressLog.invoke(null);
                Toast.makeText(context, (String) value, Toast.LENGTH_LONG).show();
            } catch (Exception ignore) {

            }
        }
    }
}
