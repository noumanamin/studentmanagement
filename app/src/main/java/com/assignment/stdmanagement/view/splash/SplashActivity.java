package com.assignment.stdmanagement.view.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.assignment.stdmanagement.view.home.HomeActivity;
import com.assignment.stdmanagement.R;

public class SplashActivity extends AppCompatActivity {


    /** Duration of wait in milliseconds**/
    private final int SPLASH_DISPLAY_TIME = 2000;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_splash);

        /* New Handler to start the Home-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the HomeActivity. */
                Intent homeIntent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(homeIntent);
                finish();
            }
        }, SPLASH_DISPLAY_TIME);
    }
}
