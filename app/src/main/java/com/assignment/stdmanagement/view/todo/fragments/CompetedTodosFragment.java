package com.assignment.stdmanagement.view.todo.fragments;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.adapter.TodoAdapter;
import com.assignment.stdmanagement.listener.OnStatusChangeListener;
import com.assignment.stdmanagement.view.todo.addEditTodo.AddEditTodoActivity;
import com.assignment.stdmanagement.databinding.FragmentTodosBinding;
import com.assignment.stdmanagement.listener.OnItemClickListener;
import com.assignment.stdmanagement.model.Todo;
import com.assignment.stdmanagement.viewModel.TodoViewModel;

import java.lang.ref.WeakReference;
import java.util.List;

import static com.assignment.stdmanagement.view.todo.TodoActivity.EDIT_TODO_RESULT;
import static com.assignment.stdmanagement.view.todo.TodoActivity.KEY_TODO;

public class CompetedTodosFragment extends Fragment implements OnItemClickListener, OnStatusChangeListener {
    private TodoViewModel todoViewModel;
    private TodoAdapter todoAdapter;
    private FragmentTodosBinding binding;

    public CompetedTodosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_todos, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        todoViewModel = ViewModelProviders.of(this).get(TodoViewModel.class);
        binding.setLifecycleOwner(this);
        todoViewModel.setActivity(new WeakReference<Activity>(getActivity()));
        setTodosObserver();
        setTodoAdapter(binding.rvTodos);
        addSwipeTouchListener(binding.rvTodos);
    }

    private void addSwipeTouchListener(RecyclerView rvTodos) {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                todoViewModel.delete(todoAdapter.getTodo(viewHolder.getAdapterPosition()));
                Toast.makeText(getActivity(), "Todo delete", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(rvTodos);
    }

    private void setTodosObserver() {
        todoViewModel.getCompletedTodos().observe(this, new Observer<List<Todo>>() {
            @Override
            public void onChanged(@Nullable List<Todo> todos) {
                todoAdapter.updateTodos(todos);
            }
        });
    }

    private void setTodoAdapter(RecyclerView rvStudent) {
        rvStudent.setHasFixedSize(true);
        rvStudent.setLayoutManager(new LinearLayoutManager(getActivity()));
        todoAdapter = new TodoAdapter(todoViewModel.getTodos().getValue(), this, this);
        rvStudent.setAdapter(todoAdapter);
    }

    @Override
    public void onClick(Object todo) {
        Intent intent = new Intent(getActivity(), AddEditTodoActivity.class);
        intent.putExtra(KEY_TODO, (Todo) todo);
        getActivity().startActivityForResult(intent, EDIT_TODO_RESULT);
    }

    @Override
    public void onSelection(List selected) {

    }

    @Override
    public void onStatusChange(Todo todo, boolean isComplete) {
        todo.setStatus(isComplete);
        todoViewModel.update(todo);
    }
}