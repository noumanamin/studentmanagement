package com.assignment.stdmanagement.view.student.addEditStudent;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.databinding.ActivityAddEditStudentBinding;
import com.assignment.stdmanagement.listener.TextChangeListener;
import com.assignment.stdmanagement.model.Student;
import com.assignment.stdmanagement.viewModel.AddEditStudentViewModel;

import static com.assignment.stdmanagement.view.student.StudentActivity.KEY_STUDENT;

public class AddEditStudentActivity extends AppCompatActivity implements TextChangeListener {

    private AddEditStudentViewModel addEditStudentViewModel;
    private ActivityAddEditStudentBinding activityAddEditStudentBinding;
    boolean isUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityAddEditStudentBinding = DataBindingUtil.
                setContentView(this, R.layout.activity_add_edit_student);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        activityAddEditStudentBinding.setLifecycleOwner(this);
        addEditStudentViewModel = ViewModelProviders.of(this).get(AddEditStudentViewModel.class);
        if (getIntent().getExtras() == null) {
            setTitle("Add Student");
        } else {
            setTitle("Edit Student");
            activityAddEditStudentBinding.setStudent(getStudent());
        }
        addEditStudentViewModel.setTextChangeListener(this);
        addEditStudentViewModel.setActivityAddEditStudentBinding(activityAddEditStudentBinding);
        addStudentTextWatcher(activityAddEditStudentBinding.edtStudentId);
    }

    private Student getStudent() {
        if (getIntent().getExtras() != null) {
            isUpdate = true;
            return (Student) getIntent().getExtras().getSerializable(KEY_STUDENT);
        }
        return null;
    }

    private void addStudentTextWatcher(EditText edtStudentId) {
        edtStudentId.addTextChangedListener(addEditStudentViewModel.getStudentIdTextWatcher());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                if (addEditStudentViewModel.isStudentValid()) {
                    Intent intent = new Intent();
                    if (isUpdate) {
                        Student student = getStudent();
                        intent.putExtra(KEY_STUDENT, addEditStudentViewModel.getStudent(student != null ? student.getId() : 0));
                    } else {
                        intent.putExtra(KEY_STUDENT, addEditStudentViewModel.getStudent());
                    }
                    setResult(RESULT_OK, intent);
                    finish();

                } else {
                    activityAddEditStudentBinding.edtStudentId.setError("Enter StudentId");
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void onTextChanged(boolean isStudentExist) {
        if (isStudentExist && !isUpdate) {
            activityAddEditStudentBinding.edtStudentId.setError("Already Exist");
        } else {
            activityAddEditStudentBinding.edtStudentId.setError(null);
        }
    }
}
