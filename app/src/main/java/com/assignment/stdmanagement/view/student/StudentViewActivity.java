package com.assignment.stdmanagement.view.student;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MenuItem;
import android.widget.Toast;

import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.adapter.ExamsAdapter;
import com.assignment.stdmanagement.databinding.ActivityStudentViewBinding;
import com.assignment.stdmanagement.listener.OnItemClickListener;
import com.assignment.stdmanagement.model.Exam;
import com.assignment.stdmanagement.model.Student;
import com.assignment.stdmanagement.viewModel.ExamViewModel;

import java.lang.ref.WeakReference;
import java.util.List;

import static com.assignment.stdmanagement.view.student.StudentActivity.KEY_STUDENT;

public class StudentViewActivity extends AppCompatActivity implements OnItemClickListener {

    public static final String KEY_EXAM = "student_exam";
    public static final int ADD_EXAM_RESULT = 105;
    public static final int EDIT_EXAM_RESULT = 106;

    private ExamViewModel  examViewModel;
    private ExamsAdapter examsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityStudentViewBinding studentViewBinding = DataBindingUtil.
                setContentView(this, R.layout.activity_student_view);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        studentViewBinding.setStudent(getStudent());

        examViewModel = ViewModelProviders.of(this).get(ExamViewModel.class);
        examViewModel.setActivity(new WeakReference<Activity>(this));
        examViewModel.setStudent(getApplication(),getStudent());
        studentViewBinding.setExamViewModel(examViewModel);
        setExamsAdapter(studentViewBinding.rvExams);
        addSwipeTouchListener(studentViewBinding.rvExams);
        setStudentObserver();
    }

    private Student getStudent() {
        if (getIntent().getExtras() != null) {
            return (Student) getIntent().getExtras().getSerializable(KEY_STUDENT);
        }
        return null;
    }

    private void setExamsAdapter(RecyclerView rvExams) {
        rvExams.setHasFixedSize(true);
        rvExams.setLayoutManager(new LinearLayoutManager(this));
        examsAdapter = new ExamsAdapter(examViewModel.getExams(getStudent().getStudentId()).getValue(), this);
        rvExams.setAdapter(examsAdapter);
    }

    private void setStudentObserver() {
        examViewModel.getExams(getStudent().getStudentId()).observe(this, new Observer<List<Exam>>() {
            @Override
            public void onChanged(@Nullable List<Exam> students) {
                if(examsAdapter != null) {
                    examsAdapter.updateExam(students);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(Object object) {

    }

    @Override
    public void onSelection(List selected) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == ADD_EXAM_RESULT){
                Exam exam = null;
                if (data != null) {
                    exam = (Exam) data.getSerializableExtra(KEY_EXAM);
                }
                examViewModel.insert(exam);
            }else if(requestCode == EDIT_EXAM_RESULT){
                Exam exam = null;
                if (data != null) {
                    exam = (Exam) data.getSerializableExtra(KEY_EXAM);
                }
                examViewModel.update(exam);
            }
        }
    }

    private void addSwipeTouchListener(RecyclerView rvStudents) {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                examViewModel.delete(examsAdapter.getExam(viewHolder.getAdapterPosition()));
                Toast.makeText(StudentViewActivity.this, "Student delete", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(rvStudents);
    }
}
