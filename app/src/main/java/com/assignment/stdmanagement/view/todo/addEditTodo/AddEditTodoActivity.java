package com.assignment.stdmanagement.view.todo.addEditTodo;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.databinding.ActivityAddEditTodoBinding;
import com.assignment.stdmanagement.model.Todo;
import com.assignment.stdmanagement.viewModel.AddEditTodoViewModel;

import static com.assignment.stdmanagement.view.todo.TodoActivity.KEY_TODO;

public class AddEditTodoActivity extends AppCompatActivity {

    private AddEditTodoViewModel addEditTodoViewModel;
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityAddEditTodoBinding activityAddEditTodoBinding = DataBindingUtil.
                setContentView(this, R.layout.activity_add_edit_todo);
        activityAddEditTodoBinding.setLifecycleOwner(this);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        if (getIntent().getExtras() == null) {
            setTitle("Add Todo");
        } else {
            setTitle("Edit Todo");
            isEdit = true;
            activityAddEditTodoBinding.setTodo(getTodo());
        }

        addEditTodoViewModel = ViewModelProviders.of(this).get(AddEditTodoViewModel.class);
        addEditTodoViewModel.setActivityAddEditTodoBinding(activityAddEditTodoBinding);
    }

    private Todo getTodo() {
        if (getIntent().getExtras() != null) {
            return (Todo) getIntent().getExtras().getSerializable(KEY_TODO);
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                Intent intent = new Intent();
                Todo todo = addEditTodoViewModel.getTodo();
                if(isEdit) {
                    todo.setId(getTodo().getId());
                }
                intent.putExtra(KEY_TODO, todo);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
