package com.assignment.stdmanagement.view.todo;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.assignment.stdmanagement.R;
import com.assignment.stdmanagement.adapter.ViewPagerAdapter;
import com.assignment.stdmanagement.databinding.ActivityTodoBinding;
import com.assignment.stdmanagement.model.Todo;
import com.assignment.stdmanagement.view.todo.fragments.CompetedTodosFragment;
import com.assignment.stdmanagement.view.todo.fragments.UnCompetedTodosFragment;
import com.assignment.stdmanagement.viewModel.TodoViewModel;

import java.lang.ref.WeakReference;

public class TodoActivity extends AppCompatActivity {
    public static final int ADD_TODO_RESULT = 102;
    public static final int EDIT_TODO_RESULT = 103;
    public static final String KEY_TODO = "todo_activity_todo";
    private TodoViewModel todoViewModel;
    private ActivityTodoBinding activityTodoBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityTodoBinding = DataBindingUtil.
                setContentView(this, R.layout.activity_todo);
        setTitle("Todos");
        setSupportActionBar(activityTodoBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        todoViewModel = ViewModelProviders.of(this).get(TodoViewModel.class);
        todoViewModel.setActivity(new WeakReference<Activity>(this));
        activityTodoBinding.setTodoViewModel(todoViewModel);
        setupViewPager(activityTodoBinding.viewpager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CompetedTodosFragment(), "Complete");
        adapter.addFragment(new UnCompetedTodosFragment(), "Un Complete");
        viewPager.setAdapter(adapter);
        activityTodoBinding.tabs.setupWithViewPager(activityTodoBinding.viewpager);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_TODO_RESULT) {
                Todo todo = null;
                if (data != null) {
                    todo = (Todo) data.getSerializableExtra(KEY_TODO);
                }
                todoViewModel.insert(todo);
            } else if (requestCode == EDIT_TODO_RESULT) {
                Todo todo = null;
                if (data != null) {
                    todo = (Todo) data.getSerializableExtra(KEY_TODO);
                }
                todoViewModel.update(todo);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteAll:
                if (activityTodoBinding.viewpager.getCurrentItem() == 0) {
                    todoViewModel.deleteAllTodos(true);
                } else {
                    todoViewModel.deleteAllTodos(false);
                }
                Toast.makeText(this, "Student students has deleted", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
